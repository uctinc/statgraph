# Statgraph

This project visualizes the output given by
[statbot](https://gitlab.com/uctinc/statbot) as a cute little React app. It
really didn't need to be a React app, but hey, we all make mistakes, right?
Regardless, it works like it needs to.

## Installing dependencies

`npm install` will install dependencies which are needed.

## Running for development

`npm start` spins up a development server for you to use with your web browser.

## Building for deployment

I'm told `npm run build` works well for this.
