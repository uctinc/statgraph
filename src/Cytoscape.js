import React, {Component} from 'react';
import cytoscape from 'cytoscape';
import popper from 'cytoscape-popper';
import cycola from 'cytoscape-cola';

cytoscape.use(popper)
cytoscape.use(cycola)

class Cytoscape extends Component {
    cy = null
    getCyID() {
        return this.props.containerID || 'cy'
    }

    getContainer() {
        return this.container
    }

    defaultStyle() {
        return [
            {
                selector: 'node',
                css: {
                    'content': function (ele) { return ele.data('label') || ele.data('id') },
                    'text-valign': 'center',
                    'text-halign': 'center'
                }
            },
            {
                selector: '$node > node',
                css: {
                    'padding-top': '10px',
                    'padding-left': '10px',
                    'padding-bottom': '10px',
                    'padding-right': '10px',
                    'text-valign': 'top',
                    'text-halign': 'center',
                    'background-color': '#bbb'
                }
            },
            {
                selector: 'edge',
                css: {
                    'target-arrow-shape': 'triangle'
                }
            },
            {
                selector: ':selected',
                css: {
                    'background-color': 'black',
                    'line-color': 'black',
                    'target-arrow-color': 'black',
                    'source-arrow-color': 'black'
                }
            }
        ]
    }

    style() {
        return this.props.style || this.defaultStyle();
    }

    styleContainer() {
        return Object.assign({height: "100%", width: "100%", display: "block"}, this.props.styleContainer || {})
    }
    
    elements() {
        return this.props.elements || {};
    }

    layout() {
        return this.props.layout || { name: 'cola' };
    }
    
    cytoscapeOptions() {
        return this.props.cytoscapeOptions || {};
    }

    options() {
        return Object.assign({
            container: this.getContainer(),
            style: this.style(),
            elements: this.elements(),
            layout: this.layout()
        }, this.cytoscapeOptions())
    }

    componentDidMount(){
        this.cy = cytoscape(this.options())
        if (this.props.cyRef) {
            this.props.cyRef(this.cy)
        }
    }

    shouldComponentUpdate(){
        return false;
    }

    componentWillUnmount(){
        this.cy.destroy();
    }

    getCy(){
        return this.cy;
    }

    render(){
        return <div className="graph" id={this.getCyID()} style={this.styleContainer()} ref={(elt) => this.container = elt}/>
    }
}

export default Cytoscape;
