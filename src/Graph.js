import React, { Component } from 'react';
import tippy from 'tippy.js';
import Cytoscape from './Cytoscape.js';

class Graph extends Component {
    componentDidMount() {
        this.retrieveState()
        this.timer = setInterval(() => this.retrieveState(), 1000);
    }

    componentWillUnmount() {
        clearInterval(this.timer)
        this.timer = null
    }

    retrieveState() {
        fetch("http://172.24.5.1/api")
            .then(result => result.json())
            .then(result => this.setState(result))
    }

    render() {
        if (this.state == null || !this.state.hasOwnProperty("nodes")) {
            return <h1>Waiting for information...</h1>
        }
        
        var elements = []

        for (const nid in this.state.nodes) {
            const node = this.state.nodes[nid]
            const location = node.address === "MYSELF" ? "local" : `${node.address}:${node.port}`
            elements.push({
                data: {
                    id: node.identifier,
                    label: `${node.identifier}`,
                    full_label: `${location} : ${node.owned_subnets.join(', ')}`
                }
            })
        }

        for (const eid in this.state.edges) {
            const edge = this.state.edges[eid]
            elements.push({
                data: {
                    source: edge.from,
                    target: edge.to,
                    weight: edge.weight,
                    full_label: `${edge.weight}`
                }
            })
        }

        const cyRef = (cy) => {
            var makeTippy = function(node) {
                const t = tippy(node.popperRef(), {
                    html: () => {
                        var div = document.createElement('div')
                        div.innerHTML = node.data('full_label')
                        return div
                    },
                    trigger: 'manual'
                }).tooltips[0]

                node.on('mouseover', () => t.show())
                node.on('mouseout', () => t.hide())
            }

            for (let nid = 0; nid < cy.nodes().length; nid++) {
                const n = cy.nodes()[nid]
                makeTippy(n)

                const edges = n.connectedEdges()
                for (let eid = 0; eid < edges.length; eid++) {
                    const e = edges[eid]
                    makeTippy(e)
                }
            }
        }

        const layout = {
            name: 'cola',
            edgeLength: (edge) => 100 * Math.log(edge.data('weight')) / Math.log(6)
        }

        return <Cytoscape
            elements={elements}
            style={[
                {
                    selector: 'node',
                    style: {
                        'label': 'data(label)'
                    }
                },
                {
                    selector: 'edge',
                    style: {
                        'curve-style': 'bezier',
                        'target-arrow-shape': 'vee',
                    }
                }
            ]}
            layout={layout}
            cyRef={cyRef}/>
    }
}

export default Graph;
