import React, { Component } from 'react';
import './App.css';

import Graph from './Graph.js';

class App extends Component {
  render() {
    return <Graph/>
  }
}

export default App;
